Source: ooniprobe
Maintainer: Internet Measurement Packaging Team <pkg-netmeasure-discuss@lists.alioth.debian.org>
Uploaders: Iain R. Learmonth <irl@debian.org>
Section: contrib/net
Priority: extra
Build-Depends: debhelper (>= 9),
               dh-python,
               dh-systemd,
               python-all,
               python-docutils (>= 0.9.1),
               python-geoip,
               python-ipaddr,
               python-mock,
               python-openssl,
               python-scapy,
               python-setuptools,
               python-twisted (>= 12.2.0),
               python-txsocksx,
               python-txtorcon (>= 0.14.2),
               python-yaml,
               python-zope.interface,
               python-klein,
               python-certifi,
               tor
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/cgit/collab-maint/ooniprobe.git
Vcs-Git: https://anonscm.debian.org/git/collab-maint/ooniprobe.git
Homepage: https://ooni.torproject.org/
X-Python-Version: >= 2.7

Package: ooniprobe
Architecture: all
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${python:Depends},
         adduser,
         tor,
         geoip-database
Recommends: python-dumbnet,
            python-pypcap
Suggests: geoip-database-contrib,
          obfsproxy
Description: probe for the Open Observatory of Network Interference (OONI)
 OONI, the Open Observatory of Network Interference, is a global observation
 network which aims to collect high quality data using open methodologies
 and free software to share observations and data about the various types,
 methods, and amounts of network tampering in the world.
 .
 ooniprobe is a program to probe a network and collect data for the OONI
 project. It will test the current network for signs of surveillance and
 censorship.
